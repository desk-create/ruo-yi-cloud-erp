DROP DATABASE IF EXISTS `ry-product`;

CREATE DATABASE  `ry-product` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

USE `ry-product`;

-- for AT mode you must to init this sql for you business database. the seata server not need it.
CREATE TABLE IF NOT EXISTS undo_log
(
    branch_id     BIGINT(20)   NOT NULL COMMENT 'branch transaction id',
    xid           VARCHAR(100) NOT NULL COMMENT 'global transaction id',
    context       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    rollback_info LONGBLOB     NOT NULL COMMENT 'rollback info',
    log_status    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    log_created   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    log_modified  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY ux_undo_log (xid, branch_id)
    ) ENGINE = InnoDB COMMENT ='AT transaction mode undo table';

-- ----------------------------
-- 商品表
-- ----------------------------
create table prod_product
(
    product_id                  bigint(20)          not null                comment '商品ID',
    tenant_id                   varchar(20)         default '000000'        comment '租户编号【sys_tenant.tenant_id】',
    del_flag                    char(1)             default '0'             comment '删除标志（0代表存在 2代表删除）',
    create_dept                 bigint(20)          default null            comment '创建部门【sys_dept.dept_id】',
    create_by                   bigint(20)          default null            comment '创建者【sys_user.user_id】',
    create_time                 datetime                                    comment '创建时间',
    update_by                   bigint(20)          default null            comment '更新者【sys_user.user_id】',
    update_time                 datetime                                    comment '更新时间',
    remark                      varchar(500)        default null            comment '备注',
    primary key (product_id)
) engine=innodb comment = '商品表';
