
-- for AT mode you must to init this sql for you business database. the seata server not need it.
CREATE TABLE IF NOT EXISTS undo_log
(
    branch_id     BIGINT(20)   NOT NULL COMMENT 'branch transaction id',
    xid           VARCHAR(100) NOT NULL COMMENT 'global transaction id',
    context       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    rollback_info LONGBLOB     NOT NULL COMMENT 'rollback info',
    log_status    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    log_created   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    log_modified  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY ux_undo_log (xid, branch_id)
    ) ENGINE = InnoDB COMMENT ='AT transaction mode undo table';

-- ----------------------------
-- 语言表
-- ----------------------------
create table i18n_language
(
    language_id                 bigint(20)          not null                comment '语言ID',
    tenant_id                   varchar(20)         default '000000'        comment '租户编号【sys_tenant.tenant_id】',
    language_code               varchar(255)        not null                comment '语言代码',
    language_name               varchar(255)        not null                comment '语言名称',
    primary key (language_id)
) engine=innodb comment = '语言表';

-- ----------------------------
-- 国际化译文表
-- ----------------------------
create table i18n_translation
(
    translation_id              bigint(20)          not null                comment '译文ID',
    tenant_id                   varchar(20)         default '000000'        comment '租户编号【sys_tenant.tenant_id】',
    app_name                    varchar(255)        not null                comment '应用服务名',
    table_name                  varchar(255)        not null                comment '表名',
    field_name                  varchar(255)        not null                comment '字段名',
    data_id                     bigint(20)          default null            comment '数据ID',
    language_code               varchar(255)        not null                comment '语言代码【i18n_language.language_code】',
    translated_text             longtext                                    comment '翻译文本',
    primary key (translation_id),
    unique key `数据ID语言唯一` (data_id,language_code)
) engine=innodb comment = '国际化译文表';

