DROP DATABASE IF EXISTS `ry-customer`;

CREATE DATABASE  `ry-customer` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

USE `ry-customer`;

-- for AT mode you must to init this sql for you business database. the seata server not need it.
CREATE TABLE IF NOT EXISTS undo_log
(
    branch_id     BIGINT(20)   NOT NULL COMMENT 'branch transaction id',
    xid           VARCHAR(100) NOT NULL COMMENT 'global transaction id',
    context       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    rollback_info LONGBLOB     NOT NULL COMMENT 'rollback info',
    log_status    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    log_created   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    log_modified  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY ux_undo_log (xid, branch_id)
    ) ENGINE = InnoDB COMMENT ='AT transaction mode undo table';
-- ----------------------------
-- 客户表
-- ----------------------------
create table cus_customer
(
    customer_id                 bigint(20)          not null                comment '客户ID',
    tenant_id                   varchar(20)         default '000000'        comment '租户编号【sys_tenant.tenant_id】',
    parent_id                   bigint(20)          default '0'             comment '父ID【cus_customer.customer_id】',
    name                        char(64)            default ''              comment '名称',
    alias                       char(64)            default ''              comment '别名',
    avatar                      bigint(20)          default null            comment '头像',
    subject_type                char(64)            default 'PERSON'        comment '主体类型：企业-COMPANY、自然人-PERSON【字典cus_customer_subject_type】',
    position_name               char(32)            default ''              comment '职位',
    gender                      varchar(64)         default '2'             comment '性别 未知-2 男性-0 女性-1【字典sys_user_sex】',
    age                         bigint(20)          default '0'             comment '年龄',
    id_card                     varchar(255)        default null            comment '身份证号',
    birthday                    date                default null            comment '生日',
    nationality                 varchar(255)        default null            comment '民族',
    marriage                    varchar(255)        default null            comment '婚姻',
    education                   varchar(255)        default null            comment '学历',
    dynamic_tags                varchar(255)        default null            comment '自定义标签',
    extended_attr               longtext                                    comment '扩展属性',
    sort                        smallint(6)         default '0'             comment '排序',
    del_flag                    char(1)             default '0'             comment '删除标志（0代表存在 2代表删除）',
    create_dept                 bigint(20)          default null            comment '创建部门【sys_dept.dept_id】',
    create_by                   bigint(20)          default null            comment '创建者【sys_user.user_id】',
    create_time                 datetime                                    comment '创建时间',
    update_by                   bigint(20)          default null            comment '更新者【sys_user.user_id】',
    update_time                 datetime                                    comment '更新时间',
    remark                      varchar(500)        default null            comment '备注',
    primary key (customer_id)
) engine=innodb comment = '客户表';

-- ----------------------------
-- 线索跟进记录表
-- ----------------------------
create table cus_customer_record
(
    customer_record_id          bigint(20)          not null                comment '线索跟进记录ID',
    tenant_id                   varchar(20)         default '000000'        comment '租户编号【sys_tenant.tenant_id】',
    customer_id                 bigint(20)          not null                comment '跟进的客户ID【cus_customer.customer_id】',
    follow_way_id               bigint(20)          default '0'             comment '跟进方式ID【ped_follow_way.follow_way_id】',
    content                     longtext                                    comment '跟进内容',
    sort                        smallint(6)         default '0'             comment '排序',
    record_user_id              bigint              not null                comment '跟进人ID【sys_user.user_id】',
    next_time                   datetime            default null            comment '下次联系时间',
    del_flag                    char(1)             default '0'             comment '删除标志（0代表存在 2代表删除）',
    create_dept                 bigint(20)          default null            comment '创建部门【sys_dept.dept_id】',
    create_by                   bigint(20)          default null            comment '创建者【sys_user.user_id】',
    create_time                 datetime                                    comment '创建时间',
    update_by                   bigint(20)          default null            comment '更新者【sys_user.user_id】',
    update_time                 datetime                                    comment '更新时间',
    remark                      varchar(500)        default null            comment '备注',
    primary key (customer_record_id)
) engine=innodb comment = '线索跟进记录表';

-- ----------------------------
-- 线索表
-- ----------------------------
create table cus_clues
(
    clues_id                    bigint(20)          not null                comment '线索ID',
    tenant_id                   varchar(20)         default '000000'        comment '租户编号【sys_tenant.tenant_id】',
    name                        char(64)            default ''              comment '名称',
    customer_id                 bigint(20)          not null                comment '客户ID（转成客户时）【cus_customer.customer_id】',
    avatar                      bigint(20)          default null            comment '头像',
    subject_type                char(64)            default 'PERSON'        comment '主体类型：企业-COMPANY、自然人-PERSON【字典cus_customer_subject_type】',
    position_name               char(32)            default ''              comment '职位',
    gender                      varchar(64)         default '2'             comment '性别 未知-2 男性-0 女性-1【字典sys_user_sex】',
    age                         bigint(20)          default '0'             comment '年龄',
    id_card                     varchar(255)        default null            comment '身份证号',
    birthday                    date                default null            comment '生日',
    nationality                 varchar(255)        default null            comment '民族',
    marriage                    varchar(255)        default null            comment '婚姻',
    education                   varchar(255)        default null            comment '学历',
    dynamic_tags                varchar(255)        default null            comment '自定义标签',
    flow_status                 varchar(64)         default 'CLUES'         COMMENT '流转状态：线索-CLUES、线索池-CLUES_POOL、转成客户-CUSTOMER、无效线索-INVALID【字典cus_clues_flow_status】',
    sort                        smallint(6)         default '0'             comment '排序',
    owner_user_id               bigint              default '0'             comment '负责人ID【sys_user.user_id】',
    del_flag                    char(1)             default '0'             comment '删除标志（0代表存在 2代表删除）',
    create_dept                 bigint(20)          default null            comment '创建部门【sys_dept.dept_id】',
    create_by                   bigint(20)          default null            comment '创建者【sys_user.user_id】',
    create_time                 datetime                                    comment '创建时间',
    update_by                   bigint(20)          default null            comment '更新者【sys_user.user_id】',
    update_time                 datetime                                    comment '更新时间',
    remark                      varchar(500)        default null            comment '备注',
    primary key (clues_id)
) engine=innodb comment = '线索表';

-- ----------------------------
-- 线索跟进记录表
-- ----------------------------
create table cus_clues_record
(
    clues_record_id             bigint(20)          not null                comment '线索跟进记录ID',
    tenant_id                   varchar(20)         default '000000'        comment '租户编号【sys_tenant.tenant_id】',
    clues_id                    bigint(20)          not null                comment '跟进的线索ID【cus_clues.clues_id】',
    follow_way_id               bigint(20)          default '0'             comment '跟进方式ID【ped_follow_way.follow_way_id】',
    content                     longtext                                    comment '跟进内容',
    sort                        smallint(6)         default '0'             comment '排序',
    record_user_id              bigint              not null                comment '跟进人ID【sys_user.user_id】',
    next_time                   datetime            default null            comment '下次联系时间',
    del_flag                    char(1)             default '0'             comment '删除标志（0代表存在 2代表删除）',
    create_dept                 bigint(20)          default null            comment '创建部门【sys_dept.dept_id】',
    create_by                   bigint(20)          default null            comment '创建者【sys_user.user_id】',
    create_time                 datetime                                    comment '创建时间',
    update_by                   bigint(20)          default null            comment '更新者【sys_user.user_id】',
    update_time                 datetime                                    comment '更新时间',
    remark                      varchar(500)        default null            comment '备注',
    primary key (clues_record_id)
) engine=innodb comment = '线索跟进记录表';
