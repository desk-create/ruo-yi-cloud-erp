package org.dromara.common.tenant.helper;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dromara.common.core.constant.TenantConstants;
import org.dromara.common.core.exception.ServiceException;
import org.dromara.common.mybatis.core.domain.BaseEntity;

import java.util.HashSet;
import java.util.List;

/**
 * 使用场景：查询子孙租户的数据使用。 配合TenantHelper.ignore()使用，忽略租户逻辑查询，根据前端传的租户ID列表查询数据
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class QueryTenantDescendantHelper extends BaseEntity {

    /**
     * 当前登录租户编号
     */
    private String loginTenantId;

    /**
     * 子孙租户编号列表
     */
    private List<String> descendantTenantIds;

    /**
     * 校验查询子孙租户权限数据
     * <br/>
     */
    public void checkQueryTenantDescendant() {
        this.loginTenantId = TenantHelper.getTenantId();
        if (CollUtil.isEmpty(this.descendantTenantIds)) {
            // 默认查询当前租户数据
            this.descendantTenantIds = StrUtil.isNotBlank(TenantHelper.getTenantId()) ? List.of(TenantHelper.getTenantId()) : null;
            //没有查询子孙租户ID数据，不用校验
            return;
        }
        StpUtil.checkRoleOr(TenantConstants.SUPER_ADMIN_ROLE_KEY, TenantConstants.TENANT_ADMIN_ROLE_KEY);
        if (!new HashSet<>(TenantHelper.getDescendantTenantIds()).containsAll(this.descendantTenantIds)) {
            throw new ServiceException("非法请求");
        }
    }

}
