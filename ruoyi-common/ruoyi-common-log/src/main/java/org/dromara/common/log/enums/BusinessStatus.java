package org.dromara.common.log.enums;

/**
 * 操作状态
 *
 * @author ruoyi OR www.deskcreate.com
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
