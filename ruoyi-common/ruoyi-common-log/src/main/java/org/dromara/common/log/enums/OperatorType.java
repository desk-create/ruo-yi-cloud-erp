package org.dromara.common.log.enums;

/**
 * 操作人类别
 *
 * @author ruoyi OR www.deskcreate.com
 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
