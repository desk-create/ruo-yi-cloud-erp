<h1>DeskCreateERP</h1>

## 预览

admin 账号: admin admin123

[预览地址点这里](http://103.150.11.222:21902)

## 简介

基于 [RuoYi-Cloud-Plus](https://plus-doc.dromara.org/#/ruoyi-cloud-plus/home) 的 DeskCreateERP 后端项目

注意：后端源码只开源了基础架构代码，并没有全部功能开源，需要获取演示ERP全部功能对应的后端源码，可联系作者

**作者WX号：**

```
DeskCreate
```

## 文档

[DeskCreateERP 文档可参考RuoYi-Cloud-Plus](https://plus-doc.dromara.org/#/ruoyi-cloud-plus/home)

## 数据库脚本升级，必看！！！！！！！！！！

查看RuoYi-Cloud-Plus文档安装好数据库脚本后（）

**执行ruo-yi-cloud-erp\sql\update2erp1.X目录下的脚本**

也就是说脚本安装顺序如下图
![img.png](readme/img.png)
## 功能业务

| 业务 | 功能说明 | 来源 |
| --- | --- | --- |
| 租户管理 | 系统内租户的管理 如:租户套餐、过期时间、用户数量、企业信息等 | RuoYi-Cloud-Plus |
| 租户权限包 | 系统内租户所能使用的套餐管理 如:套餐内所包含的菜单等 | RuoYi-Cloud-Plus |
| 客户端管理 | 系统内对接的所有客户端管理 如: pc端、小程序端等<br>支持动态授权登录方式 如: 短信登录、密码登录等 支持动态控制token时效 | RuoYi-Cloud-Plus |
| 用户管理 | 用户的管理配置 如:新增用户、分配用户所属部门、角色、岗位等 | RuoYi-Cloud-Plus |
| 部门管理 | 配置系统组织机构（公司、部门、小组） 树结构展现支持数据权限 | RuoYi-Cloud-Plus |
| 岗位管理 | 配置系统用户所属担任职务 | RuoYi-Cloud-Plus |
| 菜单管理 | 配置系统菜单、操作权限、按钮权限标识等 | RuoYi-Cloud-Plus |
| 角色管理 | 角色菜单权限分配、设置角色按机构进行数据范围权限划分 | RuoYi-Cloud-Plus |
| 字典管理 | 对系统中经常使用的一些较为固定的数据进行维护 | RuoYi-Cloud-Plus |
| 参数管理 | 对系统动态配置常用参数 | RuoYi-Cloud-Plus |
| 通知公告 | 系统通知公告信息发布维护 | RuoYi-Cloud-Plus |
| 操作日志 | 系统正常操作日志记录和查询 系统异常信息日志记录和查询 | RuoYi-Cloud-Plus |
| 登录日志 | 系统登录日志记录查询包含登录异常 | RuoYi-Cloud-Plus |
| 文件管理 | 系统文件展示、上传、下载、删除等管理 | RuoYi-Cloud-Plus |
| 文件配置管理 | 系统文件上传、下载所需要的配置信息动态添加、修改、删除等管理 | RuoYi-Cloud-Plus |
| 在线用户管理 | 已登录系统的在线用户信息监控与强制踢出操作 | RuoYi-Cloud-Plus |
| 定时任务 | 运行报表、任务管理(添加、修改、删除)、日志管理、执行器管理等 | RuoYi-Cloud-Plus |
| 代码生成 | 多数据源前后端代码的生成（java、html、xml、sql）支持CRUD下载 | RuoYi-Cloud-Plus |
| 系统接口 | 根据业务代码自动生成相关的api接口文档 | RuoYi-Cloud-Plus |
| 服务监控 | 监视集群系统CPU、内存、磁盘、堆栈、在线日志、Spring相关配置等 | RuoYi-Cloud-Plus |
| 缓存监控 | 对系统的缓存信息查询，命令统计等。 | RuoYi-Cloud-Plus |
| 在线构建器 | 拖动表单元素生成相应的HTML代码。 | RuoYi-Cloud-Plus |
| 使用案例 | 系统的一些功能案例 | RuoYi-Cloud-Plus |
| 锁定屏幕 | 支持用户临时有事要走开，临时锁定屏幕防止别人操作，可以输入密码进行锁定，使用时输入密码解锁 | RuoYi-Cloud-Plus |
| **企业管理** | **租户可以创建多个企业（可以理解为子租户）** | [DeskCreateERP]() |

## 文档

[DeskCreateERP 文档可参考RuoYi-Cloud-Plus](https://plus-doc.dromara.org/#/ruoyi-cloud-plus/home)

## Git 贡献提交规范

- 参考 [vue](https://github.com/vuejs/vue/blob/dev/.github/COMMIT_CONVENTION.md) 规范 ([Angular](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular))

    - `feat` 增加新功能
    - `fix` 修复问题/BUG
    - `style` 代码风格相关无影响运行结果的
    - `perf` 优化/性能提升
    - `refactor` 重构
    - `revert` 撤销修改
    - `test` 测试相关
    - `docs` 文档/注释
    - `chore` 依赖更新/脚手架配置修改等
    - `workflow` 工作流改进
    - `ci` 持续集成
    - `types` 类型定义文件更改
    - `wip` 开发中
