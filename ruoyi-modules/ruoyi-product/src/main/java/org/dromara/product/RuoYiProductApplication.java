package org.dromara.product;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;

/**
 * 商品中心
 *
 * @author 王永超
 */
@EnableDubbo
@SpringBootApplication
public class RuoYiProductApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RuoYiProductApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  商品中心模块启动成功   ლ(´ڡ`ლ)ﾞ  ");
    }
}
