package org.dromara.copymodel;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;

/**
 * 仿制品
 *
 * @author 王永超
 */
@EnableDubbo
@SpringBootApplication
public class RuoYiCopyModelApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RuoYiCopyModelApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  仿制品启动成功   ლ(´ڡ`ლ)ﾞ  ");
    }
}
