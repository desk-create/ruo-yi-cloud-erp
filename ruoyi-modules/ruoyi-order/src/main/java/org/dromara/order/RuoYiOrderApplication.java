package org.dromara.order;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;

/**
 * 订单中心服务
 *
 * @author 王永超
 */
@EnableDubbo
@SpringBootApplication
public class RuoYiOrderApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RuoYiOrderApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  订单中心服务启动成功   ლ(´ڡ`ლ)ﾞ  ");
    }
}
