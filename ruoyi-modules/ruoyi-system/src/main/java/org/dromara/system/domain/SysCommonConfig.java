package org.dromara.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dromara.common.mybatis.core.domain.BaseEntity;

/**
 * 参数配置表 sys_common_config
 *
 * @author 王永超
 */

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_common_config")
public class SysCommonConfig extends BaseEntity {

    /**
     * 参数主键
     */
    @TableId(value = "common_config_id")
    private Long commonConfigId;

    /**
     * 参数名称
     */
    private String configName;

    /**
     * 参数键名
     */
    private String configKey;

    /**
     * 参数键值
     */
    private String configValue;

    /**
     * 系统内置（Y是 N否）
     */
    private String configType;

    /**
     * 备注
     */
    private String remark;

}
