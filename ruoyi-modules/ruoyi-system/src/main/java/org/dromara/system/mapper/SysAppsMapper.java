package org.dromara.system.mapper;

import org.dromara.system.domain.SysApps;
import org.dromara.system.domain.vo.SysAppsVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 应用管理Mapper接口
 *
 * @author LionLi
 * @date 2024-09-06
 */
public interface SysAppsMapper extends BaseMapperPlus<SysApps, SysAppsVo> {

}
