package org.dromara.system.service.impl;

import org.dromara.common.core.exception.ServiceException;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.system.domain.SysMenu;
import org.dromara.system.mapper.SysMenuMapper;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.SysAppsBo;
import org.dromara.system.domain.vo.SysAppsVo;
import org.dromara.system.domain.SysApps;
import org.dromara.system.mapper.SysAppsMapper;
import org.dromara.system.service.ISysAppsService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 应用管理Service业务层处理
 *
 * @author LionLi
 * @date 2024-09-06
 */
@RequiredArgsConstructor
@Service
public class SysAppsServiceImpl implements ISysAppsService {

    private final SysAppsMapper baseMapper;
    private final SysMenuMapper menuMapper;

    /**
     * 查询应用管理
     *
     * @param appsId 主键
     * @return 应用管理
     */
    @Override
    public SysAppsVo queryById(Long appsId){
        return baseMapper.selectVoById(appsId);
    }

    /**
     * 分页查询应用管理列表
     *
     * @param bo        查询条件
     * @param pageQuery 分页参数
     * @return 应用管理分页列表
     */
    @Override
    public TableDataInfo<SysAppsVo> queryPageList(SysAppsBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysApps> lqw = buildQueryWrapper(bo);
        Page<SysAppsVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询符合条件的应用管理列表
     *
     * @param bo 查询条件
     * @return 应用管理列表
     */
    @Override
    public List<SysAppsVo> queryList(SysAppsBo bo) {
        LambdaQueryWrapper<SysApps> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysApps> buildQueryWrapper(SysAppsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysApps> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getAppsName()), SysApps::getAppsName, bo.getAppsName());
        lqw.eq(bo.getSort() != null, SysApps::getSort, bo.getSort());
        return lqw;
    }

    /**
     * 新增应用管理
     *
     * @param bo 应用管理
     * @return 是否新增成功
     */
    @Override
    public Boolean insertByBo(SysAppsBo bo) {
        SysApps add = MapstructUtils.convert(bo, SysApps.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setAppsId(add.getAppsId());
        }
        return flag;
    }

    /**
     * 修改应用管理
     *
     * @param bo 应用管理
     * @return 是否修改成功
     */
    @Override
    public Boolean updateByBo(SysAppsBo bo) {
        SysApps update = MapstructUtils.convert(bo, SysApps.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysApps entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 校验并批量删除应用管理信息
     *
     * @param ids     待删除的主键集合
     * @param isValid 是否进行有效性校验
     * @return 是否删除成功
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            if (menuMapper.exists(new LambdaQueryWrapper<SysMenu>().in(SysMenu::getAppsId, ids))){
                throw new ServiceException("请先删除应用菜单！");
            }
        }
        return baseMapper.deleteByIds(ids) > 0;
    }
}
