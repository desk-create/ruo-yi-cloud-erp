package org.dromara.system.controller.system;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.annotation.SaMode;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.lock.annotation.Lock4j;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.constant.TenantConstants;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.exception.ServiceException;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.tenant.helper.TenantHelper;
import org.dromara.common.web.core.BaseController;
import org.dromara.system.domain.bo.SysTenantBo;
import org.dromara.system.domain.vo.SysTenantVo;
import org.dromara.system.service.ISysTenantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 子租户管理
 *
 * @author Michelle.Chung
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/tenantChild")
public class SysTenantChildController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(SysTenantChildController.class);
    private final ISysTenantService tenantService;

    /**
     * 查询租户列表
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChild:list")
    @GetMapping("/list")
    public TableDataInfo<SysTenantVo> list(SysTenantBo bo, PageQuery pageQuery) {
        List<String> descendantTenantIds = tenantService.findAllDescendantTenantIds(TenantHelper.getTenantId());
        descendantTenantIds.add(TenantHelper.getTenantId());
        if (StrUtil.isBlank(bo.getParentTenantId())) {
            //默认查自己的儿子企业
            bo.setParentTenantId(TenantHelper.getTenantId());
        } else {
            if (!descendantTenantIds.contains(bo.getTenantId())) {
                log.error("租户编号：{}，不是登录人的子孙企业", bo.getTenantId());
                throw new ServiceException("非法请求");
            }
        }
        return tenantService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出租户列表
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChild:export")
    @Log(title = "租户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysTenantBo bo, HttpServletResponse response) {
        List<SysTenantVo> list = tenantService.queryList(bo);
        ExcelUtil.exportExcel(list, "租户", SysTenantVo.class, response);
    }

    /**
     * 获取租户详细信息
     *
     * @param id 主键
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChild:query")
    @GetMapping("/{id}")
    public R<SysTenantVo> getInfo(@NotNull(message = "主键不能为空")
                                  @PathVariable Long id) {
        return R.ok(tenantService.queryById(id));
    }

    /**
     * 新增子企业
     */
    @SaCheckLogin
    @Log(title = "子租户", businessType = BusinessType.INSERT)
    @Lock4j
    @RepeatSubmit()
    @PostMapping()
    public R<Void> addChildren(@Validated(AddGroup.class) @RequestBody SysTenantBo bo) {
        if (!tenantService.checkCompanyNameUnique(bo)) {
            return R.fail("新增企业'" + bo.getCompanyName() + "'失败，企业名称已存在");
        }
        if (!tenantService.checkChildrenCountLimit(TenantHelper.getTenantId())) {
            throw new ServiceException("当前租户下子企业名额不足，请联系管理员");
        }
        SysTenantVo tenant = tenantService.queryByTenantId(TenantHelper.getTenantId());
        List<String> relationChainTenantId = tenant.getRelationChainTenantId();
        relationChainTenantId.add(tenant.getTenantId());
        bo.setChildrenCount(0L);//不可创建子租户
        bo.setAccountCount(tenant.getAccountCount());
        bo.setParentTenantId(TenantHelper.getTenantId());
        bo.setExpireTime(tenantService.queryByTenantId(TenantHelper.getTenantId()).getExpireTime());
        bo.setRelationChainTenantId(relationChainTenantId);
        return toAjax(TenantHelper.ignore(() -> tenantService.insertByBo(bo)));
    }

    /**
     * 修改租户
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChild:edit")
    @Log(title = "租户", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysTenantBo bo) {
        tenantService.checkTenantAllowed(bo.getTenantId());
        if (!tenantService.checkCompanyNameUnique(bo)) {
            return R.fail("修改租户'" + bo.getCompanyName() + "'失败，公司名称已存在");
        }
        return toAjax(tenantService.updateByBo(bo));
    }

    /**
     * 状态修改
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChild:edit")
    @Log(title = "租户", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public R<Void> changeStatus(@RequestBody SysTenantBo bo) {
        tenantService.checkTenantAllowed(bo.getTenantId());
        return toAjax(tenantService.updateTenantStatus(bo));
    }

    /**
     * 删除租户
     *
     * @param ids 主键串
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChild:remove")
    @Log(title = "租户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(tenantService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    /**
     * 动态切换租户
     *
     * @param tenantId 租户ID
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @GetMapping("/dynamic/{tenantId}")
    public R<Void> dynamicTenant(@NotBlank(message = "租户ID不能为空") @PathVariable String tenantId) {
        TenantHelper.setDynamic(tenantId, true);
        return R.ok();
    }

    /**
     * 清除动态租户
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @GetMapping("/dynamic/clear")
    public R<Void> dynamicClear() {
        TenantHelper.clearDynamic();
        return R.ok();
    }


    /**
     * 同步租户套餐
     *
     * @param tenantId  租户id
     * @param packageId 套餐id
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChild:edit")
    @Log(title = "租户", businessType = BusinessType.UPDATE)
    @GetMapping("/syncTenantPackage")
    public R<Void> syncTenantPackage(@NotBlank(message = "租户ID不能为空") String tenantId,
                                     @NotNull(message = "套餐ID不能为空") Long packageId) {
        return toAjax(TenantHelper.ignore(() -> tenantService.syncTenantPackageIncludeAllDescendantTenant(tenantId, packageId)));
    }

}
