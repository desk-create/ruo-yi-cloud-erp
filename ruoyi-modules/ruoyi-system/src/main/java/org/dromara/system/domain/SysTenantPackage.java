package org.dromara.system.domain;

import com.baomidou.mybatisplus.annotation.IEnum;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.tenant.core.TenantEntity;

import java.io.Serial;

/**
 * 租户套餐对象 sys_tenant_package
 *
 * @author Michelle.Chung
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_tenant_package")
public class SysTenantPackage extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 租户套餐id
     */
    @TableId(value = "package_id")
    private Long packageId;
    /**
     * 套餐名称
     */
    private String packageName;

    /**
     * 数据源类型：超级租户-SUPERADMIN、普通租户-ADMIN
     */
    private String rootType;

    /**
     * 关联菜单id
     */
    private String menuIds;
    /**
     * 备注
     */
    private String remark;
    /**
     * 菜单树选择项是否关联显示（ 0：父子不互相关联显示 1：父子互相关联显示）
     */
    private Boolean menuCheckStrictly;
    /**
     * 状态（0正常 1停用）
     */
    private String status;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;
    /**
     * 物料日志类型枚举
     */
    @Getter
    public enum RootTypeEnum implements IEnum<String> {
        SUPERADMIN("SUPERADMIN", "超级租户"),
        ADMIN("ADMIN", "普通租户"),

        ;

        private final String code;
        private final String name;


        RootTypeEnum(String code, String name) {
            this.code = code;
            this.name = name;
        }

        @Override
        public String getValue() {
            return this.code;
        }

    }

}
