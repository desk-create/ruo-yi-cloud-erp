package org.dromara.system.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.system.domain.SysCommonConfig;
import org.dromara.system.domain.vo.SysCommonConfigVo;

/**
 * 公共参数配置 数据层
 *
 * @author 王永超
 */
public interface SysCommonConfigMapper extends BaseMapperPlus<SysCommonConfig, SysCommonConfigVo> {

}
