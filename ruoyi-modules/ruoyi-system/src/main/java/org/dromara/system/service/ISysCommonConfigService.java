package org.dromara.system.service;

import org.dromara.system.domain.SysCommonConfig;
import org.dromara.system.domain.bo.SysConfigBo;
import org.dromara.system.domain.vo.SysCommonConfigVo;
import org.dromara.system.domain.vo.SysConfigVo;

import java.util.List;

/**
 * 公共参数配置 服务层
 *
 * @author 王永超
 */
public interface ISysCommonConfigService {

    /**
     * 查询参数配置信息
     *
     * @param configId 参数配置ID
     * @return 参数配置信息
     */
    SysCommonConfigVo selectConfigById(Long configId);

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数键值
     */
    String selectConfigByKey(String configKey);


}
