package org.dromara.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dromara.common.mybatis.core.domain.BaseEntity;

import java.io.Serial;

/**
 * 应用管理对象 sys_apps
 *
 * @author LionLi
 * @date 2024-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_apps")
public class SysApps extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 应用ID
     */
    @TableId(value = "apps_id")
    private Long appsId;

    /**
     * 图标【sys_oss.oss_id】
     */
    private Long iconOssId;

    /**
     * 应用名称
     */
    private String appsName;

    /**
     * 应用默认首页
     */
    private String baseHome;

    /**
     * 排序
     */
    private Long sort;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * 备注
     */
    private String remark;


}
