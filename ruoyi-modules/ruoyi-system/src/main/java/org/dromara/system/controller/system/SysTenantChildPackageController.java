package org.dromara.system.controller.system;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.annotation.SaMode;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.constant.TenantConstants;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.utils.StreamUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.satoken.utils.LoginHelper;
import org.dromara.common.tenant.helper.TenantHelper;
import org.dromara.common.web.core.BaseController;
import org.dromara.system.domain.SysTenantPackage;
import org.dromara.system.domain.bo.SysMenuBo;
import org.dromara.system.domain.bo.SysTenantPackageBo;
import org.dromara.system.domain.vo.MenuTreeSelectVo;
import org.dromara.system.domain.vo.SysMenuVo;
import org.dromara.system.domain.vo.SysTenantPackageVo;
import org.dromara.system.service.ISysMenuService;
import org.dromara.system.service.ISysTenantPackageService;
import org.dromara.system.service.ISysTenantService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 租户套餐管理
 *
 * @author Michelle.Chung
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/tenantChild/package")
@ConditionalOnProperty(value = "tenant.enable", havingValue = "true")
public class SysTenantChildPackageController extends BaseController {

    private final ISysTenantPackageService tenantPackageService;
    private final ISysTenantService tenantService;
    private final ISysMenuService menuService;

    /**
     * 获取菜单列表
     */
    @SaCheckPermission("system:tenantChildPackage:list")
    @GetMapping("/menu/list")
    public R<List<SysMenuVo>> list(SysMenuBo menu) {
        Long packageId = tenantService.queryByTenantId(TenantHelper.getTenantId()).getPackageId();
        if (ObjectUtil.isNotEmpty(packageId)){
            SysTenantPackageVo sysTenantPackageVo = TenantHelper.ignore(() -> tenantPackageService.queryById(packageId));
            menu.setMenuIdList(StringUtils.splitTo(sysTenantPackageVo.getMenuIds(), Convert::toLong));
        }
        List<SysMenuVo> menus = menuService.selectMenuList(menu, LoginHelper.getUserId());
        return R.ok(menus);
    }

    @GetMapping(value = "/menu/tenantPackageMenuTreeselect/{packageId}")
    public R<MenuTreeSelectVo> tenantPackageMenuTreeselect(@PathVariable("packageId") Long packageId) {
        List<SysMenuVo> menus = menuService.selectMenuList(LoginHelper.getUserId());
        List<Long> checkedKeys = menuService.selectMenuListByPackageId(packageId);
        Map<Long, Map<String, List<SysMenuVo>>> appsMap = StreamUtils.groupBy2Key(menuService.selectMenuList(LoginHelper.getUserId()), SysMenuVo::getAppsId, SysMenuVo::getAppsTerminal);
        Map<String, List<Long>> appsTerminaCheckedKeys = new HashMap<>();
        for (Long appsId : appsMap.keySet()) {
            Map<String, List<SysMenuVo>> appsTerminalMap = appsMap.get(appsId);
            for (String appsTerminal : appsTerminalMap.keySet()) {
                List<SysMenuVo> sysMenuVoList = appsTerminalMap.get(appsTerminal);
                List<Long> list = StreamUtils.toList(sysMenuVoList, SysMenuVo::getMenuId);
                appsTerminaCheckedKeys.put(appsId + appsTerminal, checkedKeys.stream()
                    .filter(list::contains)
                    .collect(Collectors.toList()));
            }
        }
        MenuTreeSelectVo selectVo = new MenuTreeSelectVo();
        selectVo.setCheckedKeys(checkedKeys);
        selectVo.setMenus(menuService.buildMenuTreeSelect(menus));
        selectVo.setAppsTerminaCheckedKeys(appsTerminaCheckedKeys);
        return R.ok(selectVo);
    }

    /**
     * 查询租户套餐列表
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChildPackage:list")
    @GetMapping("/list")
    public TableDataInfo<SysTenantPackageVo> list(SysTenantPackageBo bo, PageQuery pageQuery) {
        return TenantHelper.ignore(bo, () -> tenantPackageService.queryPageList(bo, pageQuery));
    }

    /**
     * 查询租户套餐下拉选列表
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChildPackage:list")
    @GetMapping("/selectList")
    public R<List<SysTenantPackageVo>> selectList(SysTenantPackageBo bo) {
        bo.setStatus(TenantConstants.NORMAL);
        bo.setRootType(SysTenantPackage.RootTypeEnum.ADMIN.getValue());
        return R.ok(TenantHelper.ignore(bo, () -> tenantPackageService.queryList(bo)));
    }

    /**
     * 导出租户套餐列表
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChildPackage:export")
    @Log(title = "租户套餐", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysTenantPackageBo bo, HttpServletResponse response) {
        List<SysTenantPackageVo> list = tenantPackageService.queryList(bo);
        ExcelUtil.exportExcel(list, "租户套餐", SysTenantPackageVo.class, response);
    }

    /**
     * 获取租户套餐详细信息
     *
     * @param packageId 主键
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChildPackage:query")
    @GetMapping("/{packageId}")
    public R<SysTenantPackageVo> getInfo(@NotNull(message = "主键不能为空")
                                         @PathVariable Long packageId) {
        return R.ok(tenantPackageService.queryById(packageId));
    }

    /**
     * 新增租户套餐
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChildPackage:add")
    @Log(title = "租户套餐", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysTenantPackageBo bo) {
        if (!tenantPackageService.checkPackageNameUnique(bo)) {
            return R.fail("新增套餐'" + bo.getPackageName() + "'失败，套餐名称已存在");
        }
        bo.setRootType(SysTenantPackage.RootTypeEnum.ADMIN.getValue());
        return toAjax(tenantPackageService.insertByBo(bo));
    }

    /**
     * 修改租户套餐
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChildPackage:edit")
    @Log(title = "租户套餐", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysTenantPackageBo bo) {
        if (!tenantPackageService.checkPackageNameUnique(bo)) {
            return R.fail("修改套餐'" + bo.getPackageName() + "'失败，套餐名称已存在");
        }
        return toAjax(tenantPackageService.updateByBo(bo));
    }

    /**
     * 状态修改
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChildPackage:edit")
    @Log(title = "租户套餐", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public R<Void> changeStatus(@RequestBody SysTenantPackageBo bo) {
        return toAjax(tenantPackageService.updatePackageStatus(bo));
    }

    /**
     * 删除租户套餐
     *
     * @param packageIds 主键串
     */
    @SaCheckRole(value = {TenantConstants.TENANT_ADMIN_ROLE_KEY, TenantConstants.SUPER_ADMIN_ROLE_KEY}, mode = SaMode.OR)
    @SaCheckPermission("system:tenantChildPackage:remove")
    @Log(title = "租户套餐", businessType = BusinessType.DELETE)
    @DeleteMapping("/{packageIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] packageIds) {
        return toAjax(tenantPackageService.deleteWithValidByIds(Arrays.asList(packageIds), true));
    }
}
