package org.dromara.system.domain.bo;

import io.github.linpeilie.annotations.AutoMapper;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.system.domain.SysApps;

/**
 * 应用管理业务对象 sys_apps
 *
 * @author LionLi
 * @date 2024-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysApps.class, reverseConvertGenerate = false)
public class SysAppsBo extends BaseEntity {

    /**
     * 应用ID
     */
    @NotNull(message = "应用ID不能为空", groups = { EditGroup.class })
    private Long appsId;

    /**
     * 图标【sys_oss.oss_id】
     */
    @NotNull(message = "图标【sys_oss.oss_id】不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long iconOssId;

    /**
     * 应用默认首页
     */
    @NotBlank(message = "应用默认首页不能为空", groups = { AddGroup.class, EditGroup.class })
    private String baseHome;

    /**
     * 应用名称
     */
    @NotBlank(message = "应用名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appsName;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sort;

    /**
     * 备注
     */
    private String remark;


}
