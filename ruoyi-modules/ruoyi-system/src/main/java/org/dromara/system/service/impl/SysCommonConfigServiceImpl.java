package org.dromara.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.system.domain.SysCommonConfig;
import org.dromara.system.domain.vo.SysCommonConfigVo;
import org.dromara.system.mapper.SysCommonConfigMapper;
import org.dromara.system.service.ISysCommonConfigService;
import org.springframework.stereotype.Service;

/**
 * 公共参数配置 服务层实现
 *
 * @author 王永超
 */
@RequiredArgsConstructor
@Service
public class SysCommonConfigServiceImpl implements ISysCommonConfigService {

    private final SysCommonConfigMapper baseMapper;

    /**
     * 查询参数配置信息
     *
     * @param configId 参数配置ID
     * @return 参数配置信息
     */
    @Override
    @DS("master")
    public SysCommonConfigVo selectConfigById(Long configId) {
        return baseMapper.selectVoById(configId);
    }

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数key
     * @return 参数键值
     */
    @Override
    public String selectConfigByKey(String configKey) {
        SysCommonConfig retConfig = baseMapper.selectOne(new LambdaQueryWrapper<SysCommonConfig>()
            .eq(SysCommonConfig::getConfigKey, configKey));
        if (ObjectUtil.isNotNull(retConfig)) {
            return retConfig.getConfigValue();
        }
        return StringUtils.EMPTY;
    }

}
