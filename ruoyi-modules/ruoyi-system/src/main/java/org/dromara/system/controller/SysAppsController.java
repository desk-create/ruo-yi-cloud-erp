package org.dromara.system.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.common.core.utils.StreamUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.satoken.utils.LoginHelper;
import org.dromara.common.tenant.helper.TenantHelper;
import org.dromara.system.domain.SysMenu;
import org.dromara.system.domain.bo.SysMenuBo;
import org.dromara.system.domain.vo.SysMenuVo;
import org.dromara.system.domain.vo.SysTenantPackageVo;
import org.dromara.system.service.ISysMenuService;
import org.dromara.system.service.ISysTenantPackageService;
import org.dromara.system.service.ISysTenantService;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.system.domain.vo.SysAppsVo;
import org.dromara.system.domain.bo.SysAppsBo;
import org.dromara.system.service.ISysAppsService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 应用管理
 * 前端访问路由地址为:/system/apps
 *
 * @author LionLi
 * @date 2024-09-06
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/apps")
public class SysAppsController extends BaseController {

    private final ISysAppsService sysAppsService;
    private final ISysTenantPackageService tenantPackageService;
    private final ISysTenantService tenantService;
    private final ISysMenuService menuService;

    /**
     * 查询应用管理列表
     */
    @SaCheckLogin
    @GetMapping("/listInitialize")
    public R<List<SysAppsVo>> listInitialize(SysAppsBo bo) {
        List<SysAppsVo> sysAppsVoList = sysAppsService.queryList(bo);
        SysMenuBo menu = new SysMenuBo();
        Long packageId = tenantService.queryByTenantId(TenantHelper.getTenantId()).getPackageId();
        if (ObjectUtil.isNotEmpty(packageId)){
            SysTenantPackageVo sysTenantPackageVo = TenantHelper.ignore(() -> tenantPackageService.queryById(packageId));
            menu.setMenuIdList(StringUtils.splitTo(sysTenantPackageVo.getMenuIds(), Convert::toLong));
        }
        List<SysMenuVo> menus = menuService.selectMenuList(menu, LoginHelper.getUserId());
        List<SysAppsVo> appsVoList = new ArrayList<>();
        for (SysAppsVo sysAppsVo : sysAppsVoList) {
            Map<Long, Map<String, List<SysMenuVo>>> group = StreamUtils.groupBy2Key(menus, SysMenuVo::getAppsId, SysMenuVo::getAppsTerminal);
            Map<String, List<SysMenuVo>> listMap = group.get(sysAppsVo.getAppsId());
            if (CollUtil.isNotEmpty(listMap)) {
                appsVoList.add(sysAppsVo);
            }
        }
        return R.ok(appsVoList);
    }

    /**
     * 查询应用管理列表
     */
    @SaCheckLogin
    @GetMapping("/listAll")
    public R<List<SysAppsVo>> listAll(SysAppsBo bo) {
        return R.ok(sysAppsService.queryList(bo));
    }

    /**
     * 查询应用管理列表
     */
    @SaCheckPermission("system:apps:list")
    @GetMapping("/list")
    public TableDataInfo<SysAppsVo> list(SysAppsBo bo, PageQuery pageQuery) {
        return sysAppsService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出应用管理列表
     */
    @SaCheckPermission("system:apps:export")
    @Log(title = "应用管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysAppsBo bo, HttpServletResponse response) {
        List<SysAppsVo> list = sysAppsService.queryList(bo);
        ExcelUtil.exportExcel(list, "应用管理", SysAppsVo.class, response);
    }

    /**
     * 获取应用管理详细信息
     *
     * @param appsId 主键
     */
    @SaCheckPermission("system:apps:query")
    @GetMapping("/{appsId}")
    public R<SysAppsVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long appsId) {
        return R.ok(sysAppsService.queryById(appsId));
    }

    /**
     * 新增应用管理
     */
    @SaCheckPermission("system:apps:add")
    @Log(title = "应用管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysAppsBo bo) {
        return toAjax(sysAppsService.insertByBo(bo));
    }

    /**
     * 修改应用管理
     */
    @SaCheckPermission("system:apps:edit")
    @Log(title = "应用管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysAppsBo bo) {
        return toAjax(sysAppsService.updateByBo(bo));
    }

    /**
     * 删除应用管理
     *
     * @param appsIds 主键串
     */
    @SaCheckPermission("system:apps:remove")
    @Log(title = "应用管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{appsIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] appsIds) {
        return toAjax(sysAppsService.deleteWithValidByIds(List.of(appsIds), true));
    }
}
