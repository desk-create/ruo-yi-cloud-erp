package org.dromara.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import org.dromara.common.translation.annotation.Translation;
import org.dromara.common.translation.constant.TransConstant;
import org.dromara.system.domain.SysApps;

import java.io.Serial;
import java.io.Serializable;



/**
 * 应用管理视图对象 sys_apps
 *
 * @author LionLi
 * @date 2024-09-06
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysApps.class)
public class SysAppsVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private Long appsId;

    /**
     * 图标【sys_oss.oss_id】
     */
    @ExcelProperty(value = "图标【sys_oss.oss_id】")
    private Long iconOssId;

    /**
     * 图标【sys_oss.oss_id】Url
     */
    @Translation(type = TransConstant.OSS_ID_TO_URL, mapper = "iconOssId")
    private String iconOssIdUrl;
    /**
     * 应用名称
     */
    @ExcelProperty(value = "应用名称")
    private String appsName;

    /**
     * 应用默认首页
     */
    @ExcelProperty(value = "应用默认首页")
    private String baseHome;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序")
    private Long sort;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
