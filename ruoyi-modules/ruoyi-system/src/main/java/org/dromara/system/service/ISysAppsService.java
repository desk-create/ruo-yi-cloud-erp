package org.dromara.system.service;

import org.dromara.system.domain.SysApps;
import org.dromara.system.domain.vo.SysAppsVo;
import org.dromara.system.domain.bo.SysAppsBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 应用管理Service接口
 *
 * @author LionLi
 * @date 2024-09-06
 */
public interface ISysAppsService {

    /**
     * 查询应用管理
     *
     * @param appsId 主键
     * @return 应用管理
     */
    SysAppsVo queryById(Long appsId);

    /**
     * 分页查询应用管理列表
     *
     * @param bo        查询条件
     * @param pageQuery 分页参数
     * @return 应用管理分页列表
     */
    TableDataInfo<SysAppsVo> queryPageList(SysAppsBo bo, PageQuery pageQuery);

    /**
     * 查询符合条件的应用管理列表
     *
     * @param bo 查询条件
     * @return 应用管理列表
     */
    List<SysAppsVo> queryList(SysAppsBo bo);

    /**
     * 新增应用管理
     *
     * @param bo 应用管理
     * @return 是否新增成功
     */
    Boolean insertByBo(SysAppsBo bo);

    /**
     * 修改应用管理
     *
     * @param bo 应用管理
     * @return 是否修改成功
     */
    Boolean updateByBo(SysAppsBo bo);

    /**
     * 校验并批量删除应用管理信息
     *
     * @param ids     待删除的主键集合
     * @param isValid 是否进行有效性校验
     * @return 是否删除成功
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
