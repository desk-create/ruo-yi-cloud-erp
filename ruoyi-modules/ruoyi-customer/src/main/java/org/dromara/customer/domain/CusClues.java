package org.dromara.customer.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serial;

/**
 * 线索对象 cus_clues
 *
 * @author 王永超
 * @date 2024-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cus_clues")
public class CusClues extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 线索ID
     */
    @TableId(value = "clues_id")
    private Long cluesId;

    /**
     * 名称
     */
    private String name;

    /**
     * 客户ID（转成客户时）【cus_customer.customer_id】
     */
    private Long customerId;

    /**
     * 头像
     */
    private Long avatar;

    /**
     * 主体类型：企业-COMPANY、自然人-PERSON【字典cus_customer_subject_type】
     */
    private String subjectType;

    /**
     * 职位
     */
    private String positionName;

    /**
     * 性别 未知-2 男性-0 女性-1【字典sys_user_sex】
     */
    private String gender;

    /**
     * 年龄
     */
    private Long age;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 民族
     */
    private String nationality;

    /**
     * 婚姻
     */
    private String marriage;

    /**
     * 学历
     */
    private String education;

    /**
     * 自定义标签
     */
    private String dynamicTags;

    /**
     * 流转状态：线索-CLUES、线索池-CLUES_POOL、转成客户-CUSTOMER、无效线索-INVALID【字典cus_clues_flow_status】
     */
    private String flowStatus;

    /**
     * 排序
     */
    private Long sort;

    /**
     * 负责人ID【sys_user.user_id】
     */
    private Long ownerUserId;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * 备注
     */
    private String remark;


}
