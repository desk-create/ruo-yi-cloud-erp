package org.dromara.customer.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.customer.domain.bo.CusCluesBo;
import org.dromara.customer.domain.vo.CusCluesVo;
import org.dromara.customer.domain.CusClues;
import org.dromara.customer.mapper.CusCluesMapper;
import org.dromara.customer.service.ICusCluesService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 线索Service业务层处理
 *
 * @author 王永超
 * @date 2024-09-06
 */
@RequiredArgsConstructor
@Service
public class CusCluesServiceImpl implements ICusCluesService {

    private final CusCluesMapper baseMapper;

    /**
     * 查询线索
     *
     * @param cluesId 主键
     * @return 线索
     */
    @Override
    public CusCluesVo queryById(Long cluesId){
        return baseMapper.selectVoById(cluesId);
    }

    /**
     * 分页查询线索列表
     *
     * @param bo        查询条件
     * @param pageQuery 分页参数
     * @return 线索分页列表
     */
    @Override
    public TableDataInfo<CusCluesVo> queryPageList(CusCluesBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CusClues> lqw = buildQueryWrapper(bo);
        Page<CusCluesVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询符合条件的线索列表
     *
     * @param bo 查询条件
     * @return 线索列表
     */
    @Override
    public List<CusCluesVo> queryList(CusCluesBo bo) {
        LambdaQueryWrapper<CusClues> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CusClues> buildQueryWrapper(CusCluesBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CusClues> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), CusClues::getName, bo.getName());
        lqw.eq(bo.getCustomerId() != null, CusClues::getCustomerId, bo.getCustomerId());
        lqw.eq(bo.getAvatar() != null, CusClues::getAvatar, bo.getAvatar());
        lqw.eq(StringUtils.isNotBlank(bo.getSubjectType()), CusClues::getSubjectType, bo.getSubjectType());
        lqw.like(StringUtils.isNotBlank(bo.getPositionName()), CusClues::getPositionName, bo.getPositionName());
        lqw.eq(StringUtils.isNotBlank(bo.getGender()), CusClues::getGender, bo.getGender());
        lqw.eq(bo.getAge() != null, CusClues::getAge, bo.getAge());
        lqw.eq(StringUtils.isNotBlank(bo.getIdCard()), CusClues::getIdCard, bo.getIdCard());
        lqw.eq(bo.getBirthday() != null, CusClues::getBirthday, bo.getBirthday());
        lqw.eq(StringUtils.isNotBlank(bo.getNationality()), CusClues::getNationality, bo.getNationality());
        lqw.eq(StringUtils.isNotBlank(bo.getMarriage()), CusClues::getMarriage, bo.getMarriage());
        lqw.eq(StringUtils.isNotBlank(bo.getEducation()), CusClues::getEducation, bo.getEducation());
        lqw.eq(StringUtils.isNotBlank(bo.getDynamicTags()), CusClues::getDynamicTags, bo.getDynamicTags());
        lqw.eq(StringUtils.isNotBlank(bo.getFlowStatus()), CusClues::getFlowStatus, bo.getFlowStatus());
        lqw.eq(bo.getSort() != null, CusClues::getSort, bo.getSort());
        lqw.eq(bo.getOwnerUserId() != null, CusClues::getOwnerUserId, bo.getOwnerUserId());
        return lqw;
    }

    /**
     * 新增线索
     *
     * @param bo 线索
     * @return 是否新增成功
     */
    @Override
    public Boolean insertByBo(CusCluesBo bo) {
        CusClues add = MapstructUtils.convert(bo, CusClues.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCluesId(add.getCluesId());
        }
        return flag;
    }

    /**
     * 修改线索
     *
     * @param bo 线索
     * @return 是否修改成功
     */
    @Override
    public Boolean updateByBo(CusCluesBo bo) {
        CusClues update = MapstructUtils.convert(bo, CusClues.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CusClues entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 校验并批量删除线索信息
     *
     * @param ids     待删除的主键集合
     * @param isValid 是否进行有效性校验
     * @return 是否删除成功
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteByIds(ids) > 0;
    }
}
