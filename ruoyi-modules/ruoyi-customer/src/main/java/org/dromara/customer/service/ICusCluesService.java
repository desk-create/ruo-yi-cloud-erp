package org.dromara.customer.service;

import org.dromara.customer.domain.CusClues;
import org.dromara.customer.domain.vo.CusCluesVo;
import org.dromara.customer.domain.bo.CusCluesBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 线索Service接口
 *
 * @author 王永超
 * @date 2024-09-06
 */
public interface ICusCluesService {

    /**
     * 查询线索
     *
     * @param cluesId 主键
     * @return 线索
     */
    CusCluesVo queryById(Long cluesId);

    /**
     * 分页查询线索列表
     *
     * @param bo        查询条件
     * @param pageQuery 分页参数
     * @return 线索分页列表
     */
    TableDataInfo<CusCluesVo> queryPageList(CusCluesBo bo, PageQuery pageQuery);

    /**
     * 查询符合条件的线索列表
     *
     * @param bo 查询条件
     * @return 线索列表
     */
    List<CusCluesVo> queryList(CusCluesBo bo);

    /**
     * 新增线索
     *
     * @param bo 线索
     * @return 是否新增成功
     */
    Boolean insertByBo(CusCluesBo bo);

    /**
     * 修改线索
     *
     * @param bo 线索
     * @return 是否修改成功
     */
    Boolean updateByBo(CusCluesBo bo);

    /**
     * 校验并批量删除线索信息
     *
     * @param ids     待删除的主键集合
     * @param isValid 是否进行有效性校验
     * @return 是否删除成功
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
