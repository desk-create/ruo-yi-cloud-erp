package org.dromara.customer.mapper;

import org.dromara.customer.domain.CusClues;
import org.dromara.customer.domain.vo.CusCluesVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 线索Mapper接口
 *
 * @author 王永超
 * @date 2024-09-06
 */
public interface CusCluesMapper extends BaseMapperPlus<CusClues, CusCluesVo> {

}
