package org.dromara.customer.domain.bo;

import org.dromara.customer.domain.CusClues;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 线索业务对象 cus_clues
 *
 * @author 王永超
 * @date 2024-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = CusClues.class, reverseConvertGenerate = false)
public class CusCluesBo extends BaseEntity {

    /**
     * 线索ID
     */
    @NotNull(message = "线索ID不能为空", groups = { EditGroup.class })
    private Long cluesId;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 客户ID（转成客户时）【cus_customer.customer_id】
     */
    @NotNull(message = "客户ID（转成客户时）【cus_customer.customer_id】不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long customerId;

    /**
     * 头像
     */
    @NotNull(message = "头像不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long avatar;

    /**
     * 主体类型：企业-COMPANY、自然人-PERSON【字典cus_customer_subject_type】
     */
    @NotBlank(message = "主体类型：企业-COMPANY、自然人-PERSON【字典cus_customer_subject_type】不能为空", groups = { AddGroup.class, EditGroup.class })
    private String subjectType;

    /**
     * 职位
     */
    @NotBlank(message = "职位不能为空", groups = { AddGroup.class, EditGroup.class })
    private String positionName;

    /**
     * 性别 未知-2 男性-0 女性-1【字典sys_user_sex】
     */
    @NotBlank(message = "性别 未知-2 男性-0 女性-1【字典sys_user_sex】不能为空", groups = { AddGroup.class, EditGroup.class })
    private String gender;

    /**
     * 年龄
     */
    @NotNull(message = "年龄不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long age;

    /**
     * 身份证号
     */
    @NotBlank(message = "身份证号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String idCard;

    /**
     * 生日
     */
    @NotNull(message = "生日不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date birthday;

    /**
     * 民族
     */
    @NotBlank(message = "民族不能为空", groups = { AddGroup.class, EditGroup.class })
    private String nationality;

    /**
     * 婚姻
     */
    @NotBlank(message = "婚姻不能为空", groups = { AddGroup.class, EditGroup.class })
    private String marriage;

    /**
     * 学历
     */
    @NotBlank(message = "学历不能为空", groups = { AddGroup.class, EditGroup.class })
    private String education;

    /**
     * 自定义标签
     */
    @NotBlank(message = "自定义标签不能为空", groups = { AddGroup.class, EditGroup.class })
    private String dynamicTags;

    /**
     * 流转状态：线索-CLUES、线索池-CLUES_POOL、转成客户-CUSTOMER、无效线索-INVALID【字典cus_clues_flow_status】
     */
    @NotBlank(message = "流转状态：线索-CLUES、线索池-CLUES_POOL、转成客户-CUSTOMER、无效线索-INVALID【字典cus_clues_flow_status】不能为空", groups = { AddGroup.class, EditGroup.class })
    private String flowStatus;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sort;

    /**
     * 负责人ID【sys_user.user_id】
     */
    @NotNull(message = "负责人ID【sys_user.user_id】不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long ownerUserId;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = { AddGroup.class, EditGroup.class })
    private String remark;


}
