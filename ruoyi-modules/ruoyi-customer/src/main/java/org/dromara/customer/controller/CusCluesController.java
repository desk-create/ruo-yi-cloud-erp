package org.dromara.customer.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.customer.domain.vo.CusCluesVo;
import org.dromara.customer.domain.bo.CusCluesBo;
import org.dromara.customer.service.ICusCluesService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 线索
 * 前端访问路由地址为:/customer/clues
 *
 * @author 王永超
 * @date 2024-09-06
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/clues")
public class CusCluesController extends BaseController {

    private final ICusCluesService cusCluesService;

    /**
     * 查询线索列表
     */
    @SaCheckPermission("customer:clues:list")
    @GetMapping("/list")
    public TableDataInfo<CusCluesVo> list(CusCluesBo bo, PageQuery pageQuery) {
        return cusCluesService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出线索列表
     */
    @SaCheckPermission("customer:clues:export")
    @Log(title = "线索", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CusCluesBo bo, HttpServletResponse response) {
        List<CusCluesVo> list = cusCluesService.queryList(bo);
        ExcelUtil.exportExcel(list, "线索", CusCluesVo.class, response);
    }

    /**
     * 获取线索详细信息
     *
     * @param cluesId 主键
     */
    @SaCheckPermission("customer:clues:query")
    @GetMapping("/{cluesId}")
    public R<CusCluesVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long cluesId) {
        return R.ok(cusCluesService.queryById(cluesId));
    }

    /**
     * 新增线索
     */
    @SaCheckPermission("customer:clues:add")
    @Log(title = "线索", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CusCluesBo bo) {
        return toAjax(cusCluesService.insertByBo(bo));
    }

    /**
     * 修改线索
     */
    @SaCheckPermission("customer:clues:edit")
    @Log(title = "线索", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CusCluesBo bo) {
        return toAjax(cusCluesService.updateByBo(bo));
    }

    /**
     * 删除线索
     *
     * @param cluesIds 主键串
     */
    @SaCheckPermission("customer:clues:remove")
    @Log(title = "线索", businessType = BusinessType.DELETE)
    @DeleteMapping("/{cluesIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] cluesIds) {
        return toAjax(cusCluesService.deleteWithValidByIds(List.of(cluesIds), true));
    }
}
