package org.dromara.customer.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dromara.customer.domain.CusClues;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 线索视图对象 cus_clues
 *
 * @author 王永超
 * @date 2024-09-06
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = CusClues.class)
public class CusCluesVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 线索ID
     */
    @ExcelProperty(value = "线索ID")
    private Long cluesId;

    /**
     * 名称
     */
    @ExcelProperty(value = "名称")
    private String name;

    /**
     * 客户ID（转成客户时）【cus_customer.customer_id】
     */
    @ExcelProperty(value = "客户ID", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "转=成客户时")
    private Long customerId;

    /**
     * 头像
     */
    @ExcelProperty(value = "头像")
    private Long avatar;

    /**
     * 主体类型：企业-COMPANY、自然人-PERSON【字典cus_customer_subject_type】
     */
    @ExcelProperty(value = "主体类型：企业-COMPANY、自然人-PERSON【字典cus_customer_subject_type】", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "cus_customer_subject_type")
    private String subjectType;

    /**
     * 职位
     */
    @ExcelProperty(value = "职位")
    private String positionName;

    /**
     * 性别 未知-2 男性-0 女性-1【字典sys_user_sex】
     */
    @ExcelProperty(value = "性别 未知-2 男性-0 女性-1【字典sys_user_sex】", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_user_sex")
    private String gender;

    /**
     * 年龄
     */
    @ExcelProperty(value = "年龄")
    private Long age;

    /**
     * 身份证号
     */
    @ExcelProperty(value = "身份证号")
    private String idCard;

    /**
     * 生日
     */
    @ExcelProperty(value = "生日")
    private Date birthday;

    /**
     * 民族
     */
    @ExcelProperty(value = "民族")
    private String nationality;

    /**
     * 婚姻
     */
    @ExcelProperty(value = "婚姻")
    private String marriage;

    /**
     * 学历
     */
    @ExcelProperty(value = "学历")
    private String education;

    /**
     * 自定义标签
     */
    @ExcelProperty(value = "自定义标签")
    private String dynamicTags;

    /**
     * 流转状态：线索-CLUES、线索池-CLUES_POOL、转成客户-CUSTOMER、无效线索-INVALID【字典cus_clues_flow_status】
     */
    @ExcelProperty(value = "流转状态：线索-CLUES、线索池-CLUES_POOL、转成客户-CUSTOMER、无效线索-INVALID【字典cus_clues_flow_status】", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "cus_clues_flow_status")
    private String flowStatus;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序")
    private Long sort;

    /**
     * 负责人ID【sys_user.user_id】
     */
    @ExcelProperty(value = "负责人ID【sys_user.user_id】")
    private Long ownerUserId;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
