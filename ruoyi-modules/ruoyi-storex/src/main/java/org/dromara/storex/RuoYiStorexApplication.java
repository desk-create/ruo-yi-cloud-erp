package org.dromara.storex;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;

/**
 * 库房中心
 *
 * @author 王永超
 */
@EnableDubbo
@SpringBootApplication
public class RuoYiStorexApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RuoYiStorexApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  库房中心启动成功   ლ(´ڡ`ლ)ﾞ  ");
    }
}
