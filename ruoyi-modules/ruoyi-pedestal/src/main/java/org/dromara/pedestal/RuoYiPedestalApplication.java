package org.dromara.pedestal;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;

/**
 * 基础配置底座项目
 *
 * @author 王永超
 */
@EnableDubbo
@SpringBootApplication
public class RuoYiPedestalApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RuoYiPedestalApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  基础配置底座项目启动成功   ლ(´ڡ`ლ)ﾞ  ");
    }
}
