package org.dromara.aftersale;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;

/**
 * 售后中心
 *
 * @author 王永超
 */
@EnableDubbo
@SpringBootApplication
public class RuoYiAftersaleApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RuoYiAftersaleApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  售后中心模块启动成功   ლ(´ڡ`ლ)ﾞ  ");
    }
}
